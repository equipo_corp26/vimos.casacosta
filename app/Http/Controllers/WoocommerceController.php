<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Woocommerce;

class WoocommerceController extends Controller
{
    public function index()
    {
        /* BUSCAMOS PRODUCTOS EN LA BASE DE DATOS */
        $date_filter = date('20211001');
        $products = DB::table('prod_productos')
                                ->join('prod_precios', 'prod_productos.codigo', '=', 'prod_precios.codprod')
                                ->where('prod_precios.codlista',1)
                                ->where('prod_precios.orden',1)
                                ->where('prod_precios.desde',1)
                                ->where('prod_productos.fechamod','>=',$date_filter)
                                ->select('prod_productos.codigo','prod_productos.fechamod', 'prod_precios.valorventa','prod_productos.nombre')
                                ->get();
        /* SI HAY PRODUCTOS POR ACTUALIZAR SEGUIMOS */
        if($products->count() > 0)
        {
            /* ARMAMOS FILTRO DE PRODUCTOS  */
            $param_sku ='';
            foreach ($products as $product) {
                $param_sku .= $product->codigo . ',';
            }
            /* PARAMETROS DE BUSQUEDA PARA LA QUERY */
            $params = [
                'per_page' => '10',
                'sku' => $param_sku,
            ];
            /* PRODUCTOS Y TOTAL DE PAGINAS  */
            $products_wp = Woocommerce::get('products',$params);
            $products_wp_pages = Woocommerce::totalPages('products',$params);
            /* CICLO CEGUN PAGINAS */
            for ($i = 0; $i < $products_wp_pages; $i++) { 
                $update = [];
                /* CREAR ARRAY DE DATOS PARA ACTUALIZAR */
                foreach ( $products_wp as $product_wp )
                {
                    array_push( $update , [
                        'id'        => $product_wp['id'],
                        'sku'       => $products->where('codigo',$product_wp['sku'])->first()->codigo,
                        'price'     => $products->where('codigo',$product_wp['sku'])->first()->valorventa,
                    ]);
                    Log::channel('reportwp')->info('Producto SKU:'.$product_wp['sku'].' Precio del wordpress '. $product_wp['price'] . ' precio del sistema ' . $products->where('codigo',$product_wp['sku'])->first()->valorventa);
                }
                /* ACTUALIZAR PRODUCTOS */
                Woocommerce::post('products/batch', ['update' => $update]); 
                /* PARAMETROS PARA NUEVA PAGINA  */
                $products_wp = Woocommerce::get('products',$params);
                $next_page   = Woocommerce::nextPage('products',$params);
                if ($next_page) {
                    $params = [
                        'per_page' => '10',
                        'sku'      => $param_sku,
                        'page'     => $next_page,
                    ];
                    /* BSUCAR PRODCUTOS */
                    $products_wp = Woocommerce::get('products',$params);
                }else{
                    $i = $products_wp_pages;
                }
            }
        }else{
            Log::channel('reportwp')->info('No hay productos para actualizar');
        }
        return Log::channel('reportwp')->info('Finalizado proceso de actualizacion de productos del dia');
    }
}
